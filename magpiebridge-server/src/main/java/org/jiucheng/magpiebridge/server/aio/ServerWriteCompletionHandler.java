package org.jiucheng.magpiebridge.server.aio;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

import org.jiucheng.magpiebridge.server.aio.proxy.ProxyAttachment;
import org.jiucheng.magpiebridge.server.aio.proxy.ProxyReadCompletionHandler;

/**
 * 
 * @author jiucheng
 *
 */
public class ServerWriteCompletionHandler implements CompletionHandler<Integer, ServerAttachment> {
    
    public void completed(Integer result, ServerAttachment attachment) {
        if (attachment.getWriteBuffer().hasRemaining()) {
            attachment.getServer().write(attachment.getWriteBuffer(), attachment, this);
            return;
        }
        attachment.writed.compareAndSet(true, false);
        
        ProxyAttachment proxyAttachment = attachment.getProxyAttachment(); 
        if (proxyAttachment != null) {
        	proxyAttachment.setReadBuffer(ByteBuffer.allocate(8 * 1024));
        	proxyAttachment.getAsynchronousSocketChannel().read(proxyAttachment.getReadBuffer(), proxyAttachment, new ProxyReadCompletionHandler());
        }
    }

    public void failed(Throwable exc, ServerAttachment attachment) {
        attachment.writed.compareAndSet(true, false);
        exc.printStackTrace();
    }
}
